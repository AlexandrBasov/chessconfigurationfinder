package com.aimprosoft;

import java.util.ArrayList;
import java.util.List;

public class Knight extends Figure {

    private String name;

    private int position;

    public Knight() {
        this.name = "N";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public List<Integer> getBeatsCells(String[][] chessBoard, int position) {

        List<Integer> cells = new ArrayList<Integer>();

        int r = position / chessBoard.length, c = position % chessBoard[0].length;

        if(!" ".equals(chessBoard[r][c])) return null;

        for (int j = -1; j <= 1; j += 2) {

            for (int k = -1; k <= 1; k += 2) {

                try {
                    if ((!" ".equals(chessBoard[r + j][c + k * 2]) && !"*".equals(chessBoard[r + j][c + k * 2])) || ("R".equals(chessBoard[r + j][c + k * 2]) || "K".equals(chessBoard[r + j][c + k * 2]))) {
                        return null;
                    } else if (" ".equals(chessBoard[r + j][c + k * 2]) || "*".equals(chessBoard[r + j][c + k * 2])) {
                        int pos = (r + j) * chessBoard.length + (c + k * 2);
                        if (pos >= 0) cells.add(pos);
                    }
                } catch (Exception e) {
                }
                try {

                    if ((!" ".equals(chessBoard[r + j * 2][c + k]) && !"*".equals(chessBoard[r + j * 2][c + k])) || ("R".equals(chessBoard[r + j * 2][c + k]) || "K".equals(chessBoard[r + j * 2][c + k]))) {
                        return null;

                    } else if (" ".equals(chessBoard[r + j * 2][c + k]) || "*".equals(chessBoard[r + j * 2][c + k])) {

                        int pos = (r + j * 2) * chessBoard.length + (c + k);

                        if (pos >= 0) cells.add(pos);

                    }
                } catch (Exception e) {
                }
            }
        }
        return cells;
    }

}
