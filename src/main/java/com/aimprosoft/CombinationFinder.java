package com.aimprosoft;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CombinationFinder {

    public void findCombination(int rows, int cols, List<Figure> pieces) {

        int dimension = rows * cols;

        Set<ChessBoard> allCombination = new HashSet<>();

        for (int i = 0; i < dimension; i++) {

            boolean firstFigure = true;
            boolean firstCombination = true;

            Set<ChessBoard> stepCombinations = new HashSet<>();
            ChessBoard chessBoard = new ChessBoard(rows, cols);

            for (Figure figure : pieces) {

                if (firstFigure) {
                    if (chessBoard.putFigure(figure, i)) {
                        firstFigure = false;
                    }
                } else {

                    if (firstCombination) {
                        Set<ChessBoard> childCombinations = getChildCombinations(chessBoard, figure);
                        stepCombinations = childCombinations;
                        firstCombination = false;

                    } else {
                        Set<ChessBoard> allChildCombinations = new HashSet<>();
                        for (ChessBoard board : stepCombinations) {
                            Set<ChessBoard> childCombinations = getChildCombinations(board, figure);
                            allChildCombinations.addAll(childCombinations);
                        }
                        stepCombinations = allChildCombinations;
                    }
                }
            }
            allCombination.addAll(stepCombinations);
        }

        int count = 1;
        for (ChessBoard chessBoard1 : allCombination) {
            System.out.println();
            System.out.println("Combination " + count++ + ":");
            chessBoard1.show();
        }
    }

    private static Set<ChessBoard> getChildCombinations(ChessBoard chessBoard, Figure figure) {

        Set<ChessBoard> setCombinations = new HashSet<>();

        for (int i = 0; i < chessBoard.getDimension(); i++) {
            ChessBoard oldChessBoard = new ChessBoard(chessBoard);
            if (oldChessBoard.putFigure(figure, i)) {
                setCombinations.add(oldChessBoard);
            }
        }
        return setCombinations;
    }
}
