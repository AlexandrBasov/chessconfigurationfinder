package com.aimprosoft;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainGame {

    private List<Figure> figures;

    private int rows;

    private int cols;

    public MainGame() {
        this.figures = new ArrayList<>();
    }

    public void go() {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {

            System.out.println("Enter board dimension m,n (for example 5,5):");

            String dimensions = reader.readLine();

            rows = Integer.parseInt(dimensions.split(",")[0]);

            cols = Integer.parseInt(dimensions.split(",")[1]);

            System.out.println("Enter number of Kings:");
            createPieces(figures, "King", Integer.parseInt(reader.readLine()));

            System.out.println("Enter number of Rooks:");
            createPieces(figures, "Rook", Integer.parseInt(reader.readLine()));

            System.out.println("Enter number of Knights:");
            createPieces(figures, "Knight", Integer.parseInt(reader.readLine()));

            System.out.println("Enter number of Queens:");
            createPieces(figures, "Queen", Integer.parseInt(reader.readLine()));

            System.out.println("Enter number of Bishops:");
            createPieces(figures, "Bishop", Integer.parseInt(reader.readLine()));

            System.out.println("Enter number of Pawns:");
            createPieces(figures, "Pawn", Integer.parseInt(reader.readLine()));

            long startTime = System.currentTimeMillis();

            new CombinationFinder().findCombination(rows, cols, figures);

            System.out.println("\n" + (double) (System.currentTimeMillis() - startTime) / 1000 + " second");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createPieces(List<Figure> figures, String piece, Integer number) {

        if (number > 0) {
            switch (piece) {
                case "King":
                    for (int i = 0; i < number; i++) {
                        figures.add(new King());
                    }
                    break;
                case "Rook":
                    for (int i = 0; i < number; i++) {
                        figures.add(new Rook());
                    }
                    break;
                case "Knight":
                    for (int i = 0; i < number; i++) {
                        figures.add(new Knight());
                    }
                    break;
            }
        }
    }

    public static void main(String[] args) {

        new MainGame().go();
    }
}
