package com.aimprosoft;

import java.util.ArrayList;
import java.util.List;

public class King extends Figure {

    private String name;

    private int position;

    public King() {
        this.name = "K";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public List<Integer> getBeatsCells(String[][] chessBoard, int position) {
        List<Integer> cells = new ArrayList<Integer>();
        int r = position / chessBoard.length, c = position % chessBoard[0].length;
        if(!" ".equals(chessBoard[r][c])) return null;
        for (int j = 0; j < 9; j++) {
            if (j != 4) {
                try {
                    if (" ".equals(chessBoard[r - 1 + j / 3][c - 1 + j % 3]) || "*".equals(chessBoard[r - 1 + j / 3][c - 1 + j % 3])) {
                        cells.add((r - 1 + j / 3) * chessBoard.length + (c - 1 + j % 3));
                    } else return null;
                } catch (Exception ignored) {
                }
            }
        }
        return cells;
    }
}
