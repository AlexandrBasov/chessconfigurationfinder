package com.aimprosoft;

import java.util.List;

public abstract class Figure {

    private int position;

    private String name;

    public Figure(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    //return null if it met the other piece
    public abstract List<Integer> getBeatsCells(String[][] chessBoard, int position);
}
