package com.aimprosoft;

import java.util.Arrays;
import java.util.List;

public class ChessBoard {

    private String[][] chessBoard;

    private int dimension;

    private int rows, cols;

    public ChessBoard(int m, int n) {
        this.rows = m;
        this.cols = n;
        this.dimension = m * n;
        this.chessBoard = new String[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                chessBoard[i][j] = " ";
            }
        }
    }

    public ChessBoard(ChessBoard chessBoard) {
        this.chessBoard = new String[chessBoard.getRows()][chessBoard.getCols()];
        this.cols = chessBoard.getCols();
        this.rows = chessBoard.getRows();
        this.dimension = chessBoard.getDimension();
        for (int i = 0; i < chessBoard.getDimension(); i++) {
            this.chessBoard[i / rows][i % cols] = chessBoard.getChessBoard()[i / rows][i % cols];
        }
    }

    //return false if bad position
    public boolean putFigure(Figure figure, int position) {

        //This method puts figure and fill all beats cells with "*"

        List<Integer> beatsCells = figure.getBeatsCells(chessBoard, position);

        if (null == beatsCells || (!" ".equals(chessBoard[position / rows][position % cols]))) return false;

        try {

            this.chessBoard[position / rows][position % cols] = figure.getName();

            for (Integer cell : beatsCells) {

                chessBoard[cell / rows][cell % cols] = "*";

            }

        } catch (Exception e) {
            System.out.println("position is wrong!");
        }

        return true;
    }

    public String[][] getChessBoard() {
        return chessBoard;
    }

    public void setChessBoard(String[][] chessBoard) {
        this.chessBoard = chessBoard;
    }

    public void show() {
        for (String[] rows : chessBoard) {
            System.out.println(Arrays.toString(rows));
        }
    }

    public void clean() {
        for (int i = 0; i < chessBoard.length; i++) {
            for (int j = 0; j < chessBoard[0].length; j++) {
                chessBoard[i][j] = " ";
            }
        }
    }

    public void cleanFigure(int position) {
        chessBoard[position / rows][position % cols] = " ";
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getCols() {
        return cols;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    @Override
    public int hashCode() {
        int result = 0;
        for(int i=0; i<dimension; i++){
            if(!" ".equals(chessBoard[i/rows][i%cols]) && !"*".equals(chessBoard[i/rows][i%cols])){
                result += chessBoard[i/rows][i%cols].charAt(0)*i*31;
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {

        ChessBoard otherBoard = (ChessBoard) o;

        for (int i=0; i<otherBoard.toString().length(); i++){
            if (this.toString().charAt(i) != otherBoard.toString().charAt(i)){
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        String string = "";
        for(String[] rows : chessBoard){
            for(String cell: rows){
                string += cell;
            }
        }
        return string;
    }
}
