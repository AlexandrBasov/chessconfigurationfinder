package com.aimprosoft;

import java.util.ArrayList;
import java.util.List;

public class Rook extends Figure {

    private String name;

    private int position;

    public Rook() {
        this.name = "R";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public List<Integer> getBeatsCells(String[][] chessBoard, int position) {
        List<Integer> cells = new ArrayList<Integer>();
        int r = position / chessBoard.length, c = position % chessBoard[0].length;
        if(!" ".equals(chessBoard[r][c])) return null;
        int temp = 1;
        for (int j = -1; j <= 1; j += 2) {
            try {
                while (" ".equals(chessBoard[r][c + temp * j]) || "*".equals(chessBoard[r][c + temp * j])) {
                    cells.add(r * chessBoard.length + (c + temp * j));
                    temp++;
                    if (!" ".equals(chessBoard[r][c + temp * j]) && !"*".equals(chessBoard[r][c + temp * j]))
                        return null;
                }

            } catch (Exception ignored) {
            }
            temp = 1;
            try {
                while (" ".equals(chessBoard[r + temp * j][c]) || "*".equals(chessBoard[r + temp * j][c])) {
                    cells.add((r + temp * j) * chessBoard.length + c);
                    temp++;

                    if (!" ".equals(chessBoard[r + temp * j][c]) && !"*".equals(chessBoard[r + temp * j][c]))
                        return null;
                }
            } catch (Exception ignored) {
            }
            temp = 1;
        }
        return cells;
    }

}
